'use strict';

var path = require('path');

module.exports = function(controller_directory, models) {
   var controller_import = function() {
      var relative_controller_directory = path.relative(
            __dirname,
            controller_directory);
      return function(controller) {
         var relative_controller_file = path.join(
               relative_controller_directory,
               controller);
         return require('./' + relative_controller_file);
      };
   }();

   var controllers = [
   ];

   return new function() {
      this.add_routes = function(router) {
         controllers.forEach(function(controller) {
            for (var i in controller.routes) {
               var route = controller.routes[i];
               router[route.method](route.path, route.responder);
            }
         });
      };
   }();
}
