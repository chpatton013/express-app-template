'use strict';

var path = require('path');

module.exports = function(model_directory, sequelize) {
   var model_import = function(model_directory, sequelize) {
      var relative_model_directory = path.relative(__dirname, model_directory);
      return function(model) {
         var relative_model_file = path.join(relative_model_directory, model);
         return sequelize.import('./' + relative_model_file);
      };
   }(model_directory, sequelize);

   return new function() {
   }();
};
