'use strict';

var path = require('path');
var Sequelize = require('sequelize');
var Express = require('express');
var Utilities = require('./utilities');
var Config = require('./config');

var default_config_directory = path.resolve(
      process.cwd(),
      process.env.APP_DEFAULT_CONFIG_DIRECTORY || './config');
var user_config_directory = process.env.APP_USER_CONFIG_DIRECTORY;
if (user_config_directory) {
   user_config_directory = path.resolve(process.cwd(), user_config_directory);
}

var app = Express();

Config(default_config_directory, user_config_directory).then(function(config) {
   var static_directory = Utilities.project_path(config.app.static_directory);
   var model_directory = Utilities.project_path(config.app.model_directory);
   var controller_directory = Utilities.project_path(
         config.app.controller_directory);
   var view_directory = Utilities.project_path(config.app.view_directory);

   app.use(Express.static(static_directory));
   app.set('views', view_directory);
   app.set('view engine', config.app.view_engine);

   var sequelize = new Sequelize(
         config.database.name,
         config.database.username,
         config.database.password,
         {
            host: config.database.host,
            port: config.database.port,
            dialect: config.database.dialect,

            pool: {
               min: config.database.pool_min,
               max: config.database.pool_max,
               idle: config.database.pool_idle,
            },

            storage: config.database.sqlite_storage_file,
         });

   var model_include = path.relative(
         __dirname,
         Utilities.project_path('src/models'));
   var Models = require('./' + model_include);
   var models = Models(model_directory, sequelize);

   var controller_include = path.relative(
         __dirname,
         Utilities.project_path('src/controllers'));
   var Controllers = require('./' + controller_include);
   var controllers = Controllers(controller_directory, models);
   controllers.add_routes(app);

   return sequelize.sync().return(config);
}).then(function(config) {
   var server = app.listen(config.http.port, function() {
      console.log(
            '%s listening on port %d',
            config.app.name,
            server.address().port);
   });
}).catch(function(e) {
   console.error(e, e.stack.split('\n'));
});
