FROM chpatton013/ubuntu-vagrant-docker

RUN apt-get update \
 && apt-get install --assume-yes npm nodejs nodejs-legacy \
 && rm --recursive --force /var/lib/apt/lists/*
