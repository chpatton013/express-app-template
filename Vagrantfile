# -*- mode: ruby -*-
# vi: set ft=ruby :

# Non-Linux platforms require a VM host machine to run Docker. These host
# machines sometimes require modest configuration which can be achieved by
# setting `docker.vagrant_vagrantfile` to a different Vagrantfile. Some
# platform-specific configuration must also be directly applied.
class Platform
   def self.is_mac?
      self._is? "darwin"
   end

   def self.is_windows?
      self._is? "mingw32"
   end

   def self._is? platform
      # RUBY_PLATFORM: <processor>-<platform><optional extras>
      _, host, *_ = RUBY_PLATFORM.split("-")
      host.include? platform
   end
end

Vagrant.configure(2) do |config|
   config.vm.provider "docker" do |docker|
      if Platform.is_mac?
         docker.vagrant_vagrantfile = "host/Vagrantfile.mac"
      end

      docker.build_dir = "."
      docker.has_ssh = true
   end

   if Platform.is_windows?
      config.vm.communicator = "winrm"
   end
end
